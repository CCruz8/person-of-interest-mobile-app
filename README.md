# README #

### Name:Person of Interest Mobile App ###
## A mobile app inspired by the science fiction crime drama,  Person of Interest. ##

### Features: ###

  1. Numbers.
  2. Fancy UI

### Deployment Instructions: ###
  1. Copy/paste the "person-of-interest-mobile-app" folder in to your host and access the website in to your preffered browser.

### Issues: ###

  1. Mouse-down function does not work on mobile devices.

### To-do List: ###

  1. Working navigation animations
  2. Device layout compatibilities (Smallest will be Samsung S4)
  4. alternate colours!
