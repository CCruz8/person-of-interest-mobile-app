$(document).ready(function() {
  //$("#number_box").hide();

  var poiCounter = 0; //bottom counter, set outside to keep it countinous
  var numCheck = false; //checks if var "number" is shown.
  var numDelay = 5000;

  (function loop() {

    var number = 0; //random number, set inside so it resets every loop
    setInterval(function() {
      if (numCheck == true) {
        number = 0 + Math.floor(Math.random() * 9);
        $("#output1").text("# 04" + number);

        number = 100 + Math.floor(Math.random() * 999);
        $("#output2").text("-" + number + "-");

        number = 0 + Math.floor(Math.random() * 9999);
        $("#output3").text(number);

        poiCounter++;
        $("#output4").text("(" + poiCounter + ")");
      }
      console.log(numCheck);
    }, 5000);
  }());

  $("#navAll").mousedown(function() {
    numCheck = true;

    $("#circle").css({
      transform: 'rotate(-270deg)'
    });

    $("#number_box").show();
  });

  $("#navAll").mouseup(function() {
    numCheck = false;
    $("#circle").css({
      transform: 'rotate(270deg)'
    });
    $("#number_box").hide();
  });

});
